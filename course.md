# Erklärvideos
#### ...als eine andere und/oder unterstützende Form der Lehre
Mai 2017

### Was sind Erklärvideos?
Erklärvideos sind kurze Filme, meist aus Eigenproduktion, in denen Inhalte und Sachverhalte definiert und/oder erklärt werden. Beispielsweise kann erklärt werden, wie etwas funktioniert oder wie abstrakte Konzepte und Zusammenhänge dargestellt werden können. Zu Erklärvideos gehören beispielsweise auch Videotutorials, wie sie auf Youtube zu finden sind. In diesen wird explizit dazu aufgefordert eine gezeigte Tätigkeit oder Fähigkeit nachmachen zu können. 
Ein Erklärvideo kann abgegrenzt werden von Lehrvideos, da letztere mit einem hohen didaktischen und gestalterischen Aufwand verbunden sind und meist von bekannten Verlagen professionell erarbeitet und erstellt werden. Für Erklärvideos lassen sich vier Merkmale festhalten (Wolf, 2015):

1. Thematische Vielfalt: Erklärvideos sind sowohl in der Breite als auch in der Tiefe der Themen nicht begrenzt. Anders ist dies bei professionellen Lehrfilmproduktionen, die sich meist auf bestimmte Themenfelder konzentrieren.
2. Gestalterische Vielfalt: Es gibt eine breite Varianz bei der Gestaltung von Erklärvideos. Obwohl das Produktionsbudget meist fehlt oder sehr gering ist, gibt es einfach gehaltene aber auch nahezu professionelle gestaltete Erklärvideos.
3. Informeller Kommunikationsstil: Die Videos orientieren sich meist an einem informellen Kommunikationsstil und sprechen die Zuhörer/Rezipienten auf Augenhöhe an und duzen sie. Die Erklärungen werden der Zielgruppe entsprechend angepasst und teilweise humorvoll vermittelt.
4. Diversität in der Autorenschaft: Die Produzierenden von Erklärvideos reichen von Laien bis hin zu Experten, die ganze Erklärreihen publizieren. Zu einem Thema gibt es nicht nur inhaltlich unterschiedliche Erklärungen, sondern auch gestalterisch und kommunikativ eine breite Varianz.


### Einsatzgebiete von Erklärvideos
Die Frage nach dem richtigen Einsatz von Erklärvideos lässt sich nicht einfach beantworten, da immer der Kontext betrachtet werden muss. Nicht jedes Thema eignet sich dazu, in einem Erklärvideo dargestellt zu werden. Die folgenden Punkte beschreiben im Allgemeinen, wann sich der Einsatz von Erklärvideos lohnen kann und sie einen Mehrwert gegenüber klassischer Vermittlungsmethoden haben. Im Folgenden sind Einsatzgebiete kurz dargestellt:
- schnelle **Hilfe zur Selbsthilfe:** wenn man schnell wissen, wie “etwas geht”
-> gute, visualisierte Informationsaufbereitung zu einem bestimmten Themenfeld
- **im Präsenzunterricht**, 
-> um etwas zu veranschaulichen, was nur schwer in Worten und Bildern zu erklären ist oder um als Lehrender entlastet zu werden.
-> zur Abwechslung
- **Erstellung eines Lernvideos**, um etwas zu lernen und sich intensiv mit den Inhalten auseinanderzusetzen
- **Online Unterricht**
-> Blended-Learning: vor dem eigentlichen Präsenzunterricht werden den Lernenden gute Lernvideos zur Verfügung gestellt, um dann während der Präsenzzeit Fragen und Inhalte intensiv zu besprechen (“inverted classroom”, “flipped classroom”, „reversed classroom“)
Eine Abwägung der Vor- und Nachteile kann bei der Entscheidung, ob der Einsatz von Erklärvideos sinnvoll ist, helfen.


### Was brauche ich zur Erstellung von Erklärvideos
Für die Erstellung von Erklärvideos ist nicht nur ein gewisses technisches „Knowhow“ notwendig, sondern auch eine technische Ausrüstung. Je nach Umfang und Komplexität reicht die Ausrüstung von einer handelsüblichen Kamera, einer Handykamera und einem Mikrofon bis hin zu einer hochwertigen Kamera, Mikrofon und ggf. Beleuchtungsmitteln. Der damit verbundene technische und zeitliche Aufwand sollte daher nicht unterschätzt werden. Allerdings gilt zu berücksichtigen, dass ein fertig gestelltes Erklärvideo immer wieder abrufbar ist und zur Lehre genutzt werden kann. Anders als Vorlesungen, die jedes Semester aufs Neue vorgetragen werden müssen. Die Qualität der Videos ist gleichbleibend, das Video kann beliebig oft wiederholt werden und stellt die Inhalte kurz und kompakt dar. Die vielfältigen Darstellungsmöglichkeiten ermöglichen eine realitätsnahe und emotionale Wirkung auf den Rezipienten, wodurch indirekte Erfahrungen gemacht werden können, die in einem beliebigen Hörsaal nicht möglich sind. So können die Rezipienten mit zu besonderen Orten, Laboren o.a. Örtlichkeiten mitgenommen werden und erhalten Einblicke, die sonst nicht möglich sind.

### Wie geht man vor
Bei der Erstellung eines Erklärvideos gilt es im Vorfeld einige Punkte zu berücksichtigen. Man sollte nicht einfach „drauflos“ filmen, sondern ein Drehbuch für das eigene Video schreiben. In diesem Video werden alle Aspekte berücksichtigt, die die Inhalte, Darstellung und die technischen Voraussetzungen betreffen.

##### Welche Gedanken muss ich mir Vorfeld machen?
Im Vorfeld ist es empfehlenswert, einen genauen Plan zu erstellen, wie das Video aufgebaut sein soll, welche Zielgruppe und zu welchem Zweck es gedreht werden soll. Die Festlegung dieser Punkte hilft dabei, das Ziel nicht aus den Augen zu verlieren und das Video auf das Wesentliche zu beschränken (Zorn, Auwärter & Seehagen-Marx, 2011). Im Folgenden sind die wichtigsten Aspekte kurz zusammengefasst:
- Welche Zielgruppe möchte ich erreichen?

|Vorteile|Nachteile|
|-----|------|
|Kompaktheit und Kürze (effizienteres Lernen)|technischer Aufwand|
|Wiederholbarkeit|ggf. zeitlicher Aufwand |
|Bessere Organisation und Koordinierbarkeit (Zeit- und ortsunabhängig), (Mobile Learning möglich, entspannte Lernatmosphäre - z.B. wenn “Offline Ansehen” möglich ist)|ggf. kostenintensiv, aber nicht zwingend|
|Zeit- und Kostenersparnis||
|Selbstbestimmtes Lernen (Lerntempo, Lernleistung)||
|Konstante Qualität||
|Realitätsnähe (emotionale und anschauliche Wirkung, indirekte Erfahrungen ermöglichen)||
|Vielfältige Darstellungsmöglichkeiten||

- Welche Lernziele sollen mit dem Video und den darin enthaltenen Inhalten erreicht werden?	
-	Welches technische Educast-Format (zum Beispiel Audiocast, Screencast) soll zum Einsatz kommen?	
-	Welche Ressourcen (technisch, personell) sind vorhanden, welche werden benötigt?
-	Wie kann ich das Informationsmaterial auf das Wesentliche eingrenzen und strukturieren?
-	Wie gestalte ich die Lerninhalte?
-	Wie sichere und fördere ich die Motivation? (Rekapitulieren, Feedback) - dabei wieder die Zielgruppe im Blick behalten.
-	Wo möchte ich es veröffentlichen? Mit welcher Lizenz (Creative Commons,…)?

##### Vorgehensweise
Wenn ein erster Rahmen gelegt ist und die Gedanken, die man sich im Vorfeld machen muss, schriftlich festgehalten sind, kann die konkrete Planung des Videos kommen. 

**a) Die Zielsetzung**
-	Was soll im Video gelernt werden, was soll damit vermittelt werden? (Lernziele)
-	Warum ist ein Video hier die richtige Wahl (vielleicht gibt es ja eine viel weniger aufwändige und passende Alternative)?
-	Wer ist die Zielgruppe genau? Welches Lernvideo ist warum für Sie geeignet? Wie lang soll das Video werden?
-	Was sind Kernaussagen des Lernvideos, die ggf. auch wiederholt werden sollten oder auf unterschiedliche Arten erklärt werden müssen? (Inspirieren lassen, recherchieren, usw.)

**b) Der Inhalt**
-	Die Informationen sollten nicht einfach nur benannt werden. Damit die Rezipienten so viel wie möglich aus diesem Video mitnehmen, ist es empfehlenswert die Inhalte in eine Geschichte zu packen, ein Abenteuer zu erzählen, eine Reportage, eine persönliche Geschichte, u.v.m.
-	Adäquate und anschauliche Visualisierung
-	Eher wenig Personenaufnahmen
-	Keine Ablenkung im Hintergrund
-	Lernvideo aber dennoch unterhaltsam: Forschungsfrage, Spannungsbogen (Dramaturgie), Alleinstellungsmerkmal
-	Corporate Design der Uni zu beachten?

**c) Das Drehbuch**
-	Titel: einfach, klar, unmissverständlich, assoziativ
-	Einführung: hat man direkt zu Beginn einen Plan davon, was im Video folgt?
-	Länge: zwischen 2 und 6 Minuten
-	Schluss: kurze Zusammenfassung am Ende
-	Gestaltung: Gesprochenes und Geschriebenes an Zielgruppe anpassen
-	Ggf. Zeichnungen
-	Wiederholungen: wenn hilfreich, z.B. bei Fachbegriffen (definieren, beispielhaft erklären, ...)
-	Fehlerfrei !

**d) Beachten/ Berücksichtigen**
- Abbildung von Personen oder gar Filmen von Häusern bzw. innerhalb von Wohnungen, Nutzung von fremden Materialien, Bilder, Musik (auch wenn es nur im Hintergrund per Radio gehört wird oder gar nur gepfiffen). 

**e) Produktionsmittel**
-	Kameras, Handy, Laptops, iPad
-	Auf Augenhöhe aufnehmen, aber abhängig vom Motiv (Kameraperspektive)
-	Auf Zoom/Schwenke verzichten, Stativ verwenden (ggf. Tragestativ)
-	Effekte: lieber einheitlich, nicht zu viele auf einmal, Zeitraffer
-	Gute Beleuchtung (Stichwort Softbox)
-	Guter Ton ist meistens wichtiger als das Bild, also überlegen, ob Zusatzmikrofon sinnvoll ist (Externes Mic).


##### Einbindung von Erklärvideos in die Lehre
Handke (2015) versucht eine schlüssige Klassifikation von Lehrvideos aufzustellen, die es ermöglichen soll, den Einsatz von Lehrvideos in der Lehre einordnen zu können (S. 59f). Demnach gebe es fünf Parameter, die eine Unterscheidung ermöglichen:

**1. Die Aufnahmemethode**
- Aufnahme mit einer Kamera
- Abgreifen des Computerbildschirms (Screencasts)
- Die Kombination beider Methoden

**2. Die Inhaltsvermittlung**
- Mitschnitt von klassischen Vorlesungen
- Lehrfilmen
- uvm.

**3. Der Aufnahmeort (3 Typen von Lehrvideos)**
- 	Videos im „Classroom-Setting“ (Aufnahme findet live im Vorlesungsaal/Klassenzimmer statt)
- 	Videos im „Studio-Setting“ (Aufnahme im ausgestatteten Studio)
- 	Videos im „Office-Setting“ (Aufnahme im Büro, mit PC und Schreibtisch)


**4. Die Spieldauer**
Kann von weniger als einer Minute bis hin zu 90 Minuten dauern (Vorlesung Aufnahme). Allerdings stehen die Spieldauer und Wirksamkeit in einem umgekehrten Verhältnis zueinander (Handke, 2015, S. 63). Eine Studie von Guo et al. (2014) hat bis 6 Minuten als effiziente Spieldauer festgelegt. Es besteht der Wunsch nach Kompaktheit, um so viele Informationen wie möglich in kürzester Zeit aufzunehmen.

**5.	Die Integration des Sprecherbildes (engl. „Talking Head“)**
1.	Sprecherbild integriert:
     * Personalisiert
     * Unterstützt die non-verbalen Aspekte
     * Verhindert evtl. Nutzung durch Dritte

2. 	Sprecherbild fehlt:
    * Neutral
    * Verringert die Hürde zur Nutzung durch Dritte

Bei Erklärvideos sei das Sprecherbild eher kontraproduktiv, weil es vom Inhalt ablenken könnte (wie z.B. mathematische Formeln). Der Einsatz eines Sprecherbildes ist daher stark vom Inhalt abhängig.
Handke (2015) hält zwei Parameter fest, die sich als hilfreich erweisen, Lehrvideos zu klassifizieren: der Aufnahmeort und die Spieldauer.

*Der Aufnahmeort*

##### Videos im „Classroom-Setting“
Die Aufnahme findet live im Vorlesungsaal/Klassenzimmer vor Publikum statt und ist das klassische Äquivalent. Die Präsentation kann ggf. mit aufgenommen werden als Screencast. Diese Art eignet sich als Nachbereitung von Lerninhalten, z.B. im Krankheitsfall oder dem Verpassen der Präsenzphase aus anderen Gründen
Ausstattung:
-	Kamera
-	Kameramann
-	Ggf. Personal zur Nachbearbeitung
-	Laptop
-	Mikrofon
-	Screencast Software: Camtasia Studio, Lecturnity, ProfCast, Jing
-	Videokonverter

Nachteile: 
-	Komplette Aufzeichnungen sind zu lang, Frontalvorlesung, nachträglich, keine Interaktion
-	Diverse nicht inhaltsbezogene Elemente sind enthalten, die unnötig sind: Pausen, Wiederholungen
-	Schwierigkeiten bei der Aufnahme: Beleuchtungsprobleme, Akustikprobleme
Aufgrund der Vor- und Nachteile kann festgehalten werden, dass kein signifikanter Mehrwert im Vergleich zu persönlich gehaltenen Vorlesungen besteht. Im Gegenteil sogar ein kleiner Minderwert, weil keine Interaktion mit den Lernenden möglich ist

##### Videos im "Studio-Setting"
Die Aufnahmen finden an speziell dafür hergerichteten Orten statt, sie erfordern einen hohen technischer, nahezu professionellen Aufwand und Produktionsverhältnisse. Es wird mit Hintergrundmusik und aufwändigen Effekten gearbeitet. Dabei bleibt fraglich, ob damit gewünschte Lerneffekte erreicht werden. Besonders eignen sich diese Form der Lehrvideos für Experimente.

##### Videos im "Office-Setting"
Die Videos können in einem Büro, Aufenthaltsraum, Seminarraum o.ä. erzeugt werden. Sie bieten eine realistische Möglichkeit „zur flächendeckenden Digitalisierung der Hochschullehre“ (Handke, 2015, S. 75). Dabei sollte eine Spieldauer von 15 Minuten nicht überschritten werden. In diesem Rahmen gibt es sogenannte Micro- und Marco-Teaching Videos:
Micro-Teaching Videos:
-	Selten länger als 6 Minuten, für kurze glossarähnliche Beiträge
-	Liefern einfache Erklärungen oder Musterlösungen
-	Aufgabenstellungen werden erläutern, Sachverhalte demonstriert
-	Anstelle eines einfachen Handouts/einer Präsentation/eines Tafelbildes etc.
-	Gestik und Mimik des Sprechers kann ein Vorteil sein
-	Ausführlicher als ein Text

Macro-Teaching Videos: „E-Lecture“ / „Electronic Lectures“:
-	Lehrvideos, Basis für Inhaltsvermittlung
-	Inhaltlich kompakte und geschlossene Präsentationen bis zu 20 Minuten
-	Grundlage: Skript, vorher erstellte Präsentation, die die mündlichen Erklärungen des Sprechers unterstützt

##### Einbindung in die Lehre
Die Erklärvideos können zur Vorbereitung oder reinen Präsentation von Inhalten und der Wissensvermittlung verwendet werden. Wenn die Erklärvideos jedoch optimal in die Lehre eingebunden werden, können im Anschluss an das Video Quiz- und/oder Lernfragen folgen, die die Inhalte noch einmal wiederholen und der Nachbereitung dienen. Zudem eignet sich auch der Austausch und die Diskussion über die Inhalte des Erklärvideos, nachdem das Video angeschaut wurde. Dies ist z.B. über Foren, Wikis, Chats oder andere Plattformen möglich.

#####  Beispiele
Dieser YouTube Kanal unserer Fakultät enthält fünf Erklärfilme, die durch Studierende aus dem Fachbereich Sozialpsychologie erstellt wurden und als OER (Open Educational Resources) unter der Lizenz CC0 veröffentlicht wurden:
	https://www.youtube.com/channel/UC8ymWxoyEpLRpQQyE_LT_tQ




##### Quellen:
- Handke, J. (2015). Handbuch Hochschullehre digital – Leitfaden für eine moderne und mediengerechte Lehre.
- Reinmann, G. (2012). Studientext Didaktisches Design. München. (URL: http://lernen-unibw.de/studientexte)
- Wolf, K. D. (2015). Bildungspotenziale von Erklärvideos und Tutorials auf YouTube. In Merz 1 (59), S. 30-36.
- Zorn, I.; Auwärter, A. & Seehagen-Marx, H. (2011). Educasting - Wie Podcasts in Bildungskontexten Anwendung finden. In: M. Ebner & S. Schön (Hrsg.), Lehrbuch für Lernen und Lehren mit Technologien (L3T). (URL:http://l3t.eu/homepage/das-buch/ebook/kapitel/o/id/20)

